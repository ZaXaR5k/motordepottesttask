# MotorDepotTestTask

run server command: 

`npm start`

rest api:

GET api/cars - get all cars

POST api/cars - add new car

GET api/cars/:id - get one car by id

PUT  api/cars/:id - update car by id

DELETE api/cars/:id - delete car by id



